
var App = {
    
    init : function() {
        
        this.listCtrl1 = new ListCtrl({
            products: document.querySelector("#compare-products #products1"),
            results: document.querySelector("#compare-products #results1"),
            tab: document.querySelector("#compare-products #tab1"),
            mobselect: document.querySelector("#compare-products #mobselect1")
        });
        
        this.listCtrl2 = new ListCtrl({
            products: document.querySelector("#compare-products #products2"),
            results: document.querySelector("#compare-products #results2"),
            tab: document.querySelector("#compare-products #tab2"),
            mobselect: document.querySelector("#compare-products #mobselect2")
        });
        
        this.listCtrl3 = new ListCtrl({
            products: document.querySelector("#compare-products #products3"),
            results: document.querySelector("#compare-products #results3"),
            tab: document.querySelector("#compare-products #tab3"),
            mobselect: document.querySelector("#compare-products #mobselect3")
        });
        
    }
};