
function ListCtrl(settings) {
    
    this.products = settings.products;
    this.results = settings.results;
    this.tab = settings.tab;
    this.prev = settings.mobselect.querySelector(".prev");
    this.title = settings.mobselect.querySelector("h3.title");
    this.next = settings.mobselect.querySelector(".next");
    
    this.init();
}

ListCtrl.prototype = {
    
    init : function() {
        
        var _self = this;
        this.products.addEventListener("change", function(e) {
            _self.showProduct(e.target.value);
        });        
        
        this.tab.addEventListener("click", function() {
            alert("TAB");
        });
        
        this.prev.addEventListener("click", function() {
            alert("PREV");
        });
        
        this.next.addEventListener("click", function() {
            alert("NEXT");
        });
        
    },
            
    showProduct : function(product) {

        this.clear();        
        this.results.querySelector("#" + product).className = "visible";
        
    },
            
    setTitle : function() {
        
        /* TODO */
    },
            
    clear : function() {
        
        var articles = this.results.querySelectorAll("article");
        
        for (var i = 0; i < articles.length; i++) {
            articles[i].className = "hidden";
        }
        
    }
    
};
